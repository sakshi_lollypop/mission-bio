export const utility = {
  isMobileView: function () {
    return ($(window).width() < 768);
  },

  isTabletView: function () {
    return ($(window).width() < 1200);
  }
}
